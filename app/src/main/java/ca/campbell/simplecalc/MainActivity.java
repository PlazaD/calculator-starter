package ca.campbell.simplecalc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import java.util.Locale;

public class MainActivity extends Activity {

  private EditText etNumber1;
  private EditText etNumber2;
  private TextView result;
  private double num1;
  private double num2;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    // get a handle on the text fields
    etNumber1 = (EditText) findViewById(R.id.num1);
    etNumber2 = (EditText) findViewById(R.id.num2);
    result = (TextView) findViewById(R.id.result);
  }

  public void addNums(View v) {
    if (getNums()) {
      displayNum(num1 + num2);
    }
  }

  public void subtractNums(View v) {
    if (getNums()) {
      displayNum(num1 - num2);
    }
  }

  public void multiplyNums(View v) {
    if (getNums()) {
      displayNum(num1 * num2);
    }
  }

  public void divideNums(View v) {
    if (getNums()) {
      if (num2 == 0) {
        result.setText(R.string.division_by_zero);
        return;
      }

        displayNum(num1 / num2);
    }
  }

  public void clearNums(View v) {
    etNumber1.setText("");
    etNumber2.setText("");
    result.setText(R.string.result_text);
  }


  private void displayNum(double num) {
    result.setText(String.format(Locale.CANADA, "%f", num));
  }

  private boolean getNums() {
    try {
      num1 = Double.parseDouble(etNumber1.getText().toString());
      num2 = Double.parseDouble(etNumber2.getText().toString());
    } catch (NumberFormatException e) {
      result.setText(R.string.input_nan);
      return false;
    }

    return true;
  }
}